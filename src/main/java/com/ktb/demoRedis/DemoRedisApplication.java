package com.ktb.demoRedis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRedisApplication {

	//Test Demo jenkins
	public static void main(String[] args) {
		SpringApplication.run(DemoRedisApplication.class, args);
	}

}
